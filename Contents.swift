//: Playground - noun: a place where people can play

import UIKit

// MARK: - File stuff

func getDocumentsDirectory() -> String {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
    let documentsDirectory = paths[0]
    return documentsDirectory as String
}

func listFiles() -> [String] {
    let dir:String = getDocumentsDirectory() as String
    do {
        let fileList = try FileManager.default.contentsOfDirectory(atPath: dir)
        return fileList as [String]
    }catch {
        
    }
    let fileList = [""]
    return fileList
}



print(listFiles())



// MARK: - File access


func writeFile(fName: String, content: String) -> (){
    let file = "/" + fName
    let filename = getDocumentsDirectory().appending(file as String)
    do {
        try content.write(toFile: filename, atomically: true, encoding: String.Encoding.utf8)
        print("write succa")
    } catch {
        // failed to write file – bad permissions, bad filename, missing permissions,
        //  or more likely it can't be converted to the encoding
        print("Write error")
    }
}

writeFile(fName: "A", content: "Test content")




func readFile(fName: NSString) ->(String) {
    // let filename = getDocumentsDirectory().appending(fName as String)
    let DocumentDirURL = try! FileManager.default.url(
        for: .documentDirectory, in: .userDomainMask,
        appropriateFor: nil, create: true)
    let fileURL = DocumentDirURL.appendingPathComponent(fName as String)
    var inString = ""
    do {
        inString =  try String(contentsOf: fileURL)
    } catch let error as NSError {
        print("Failed reading from URL: \(fileURL), Error: " + error.localizedDescription)
    }
    return inString
}

print(readFile(fName: "A"))



